import { ApiRequest } from "../request";

export class PostsController {
    
    async newPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async likePost(likeData: object, accessToken: string) {
        const response = await new ApiRequest() 
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts/like`)
            .body(likeData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async commentPost(commentData: object, accessToken: string) {
        const response = await new ApiRequest() 
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Comments`)
            .body(commentData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    
}