import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const register = new RegisterController();
const auth = new AuthController();
const users = new UsersController();

describe("First path", () => {
    let userId: number;
    let email: string;
    let accessToken: string;

    it(`Registration`, async () => {
        let response = await register.register(0, "avatar", "MyEmail@gmail.com" , "user", "Password1");

        expect(response.statusCode, `Status code should be 201`).to.be.equal(201);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_register);  
        
        userId = response.body.user.id;
        email = response.body.user.email;  
    });

    it(`Get all users`, async () => {
        let response = await users.getAllUsers();

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);         
    });

    it(`Authorization`, async () => {
        let response = await auth.login(email, "Password1");   

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body.user.id, `Verify user id`).to.be.equal(userId); 
        expect(response.body.user.email, `Verify email`).to.be.equal(email); 
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_register);  

        accessToken = response.body.token.accessToken.token;
    });
    
    it(`Get user from token`, async () => {
        let response = await users.getUserFromToken(accessToken);

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);  
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);   
        expect(response.body.id, `Verify user id`).to.be.equal(userId); 
        expect(response.body.email, `Verify email`).to.be.equal(email);
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_userData); 

    });

    it(`Update user`, async () => {
        let userData: object = {
            id: userId,              
            avatar: "newAvatar",
            email: "myNewEmail@gmail.com",                        
            userName: "newUser",
        };
        let response = await users.updateUser(userData, accessToken);

        expect(response.statusCode, `Status code should be 204`).to.be.equal(204);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);  
    });

    it(`Get updated user from token`, async () => {
        let response = await users.getUserFromToken(accessToken);

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);  
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);   
        expect(response.body.id, `Verify user id`).to.be.equal(userId); 
        expect(response.body.email, `Verify email`).to.be.equal("myNewEmail@gmail.com");  
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_userData);            
    });

    it(`Get user by id`, async () => {
        let response = await users.getUserById(userId);

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);  
        expect(response.body.id, `Verify user id`).to.be.equal(userId); 
        expect(response.body.email, `Verify email`).to.be.equal("myNewEmail@gmail.com"); 
        expect(response.body.userName, `Verify user name`).to.be.equal("newUser"); 
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_userData); 
    });

    it(`Delete user`, async () => {
        let response = await users.deleteUser(userId, accessToken);

        expect(response.statusCode, `Status code should be 204`).to.be.equal(204);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);  
    }); 
});





