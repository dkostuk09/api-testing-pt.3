import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const register = new RegisterController();
const auth = new AuthController();
const posts = new PostsController();
const users = new UsersController();

describe("Second path", () => {
    let userId: number;
    let email: string;
    let accessToken: string;
    let postId: number;

    it(`Registration`, async () => {
        let response = await register.register(0, "avatar", "newUser1@gmail.com" , "user", "newPassword");

        expect(response.statusCode, `Status code should be 201`).to.be.equal(201);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_register);  
        
        userId = response.body.user.id;
        email = response.body.user.email;  
    });

    it(`Negative test - authorization`, async () => {
        let response = await auth.login(email, "invalidPassword");   

        expect(response.statusCode, `Status code should be 201`).to.be.equal(401);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
    });

    it(`Authorization`, async () => {
        let response = await auth.login(email, "newPassword");   

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body.user.id, `Verify user id`).to.be.equal(userId); 
        expect(response.body.user.email, `Verify email`).to.be.equal(email); 
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_register);  

        accessToken = response.body.token.accessToken.token;
    });

    it(`Placement the post`, async () => {
        let postData: object = {
            authorId: userId,              
            previewImage: "image",
            body: "hello"
        };
        let response = await posts.newPost(postData, accessToken);

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);  
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);   
        expect(response.body.author.id, `Verify user id`).to.be.equal(userId); 
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_newPost); 
        
        postId = response.body.id;
    });

    it(`Get all posts`, async () => {
        let response = await posts.getAllPosts();

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
    });
          
    it(`Like my post`, async () => {
        let likeData: object = {
            entityId: postId,              
            isLike: true,
            userId: userId
        };
        let response = await posts.likePost(likeData, accessToken);

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);         
    });

    it(`Comment my post`, async () => {
        let commentData: object = {
            authorId: userId,              
            postId: postId,
            body: "cool"
        };
        let response = await posts.commentPost(commentData, accessToken);

        expect(response.statusCode, `Status code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);
        expect(response.body.author.id, `Verify user id`).to.be.equal(userId); 
        expect(response.body, `Verify validation schema`).to.be.jsonSchema(schemas.schema_comment);         
    });

    it(`Negative test - comment`, async () => {
        let commentData: object = {
            authorId: userId,              
            postId: 959595959,
            body: "cool"
        };
        let response = await posts.commentPost(commentData, accessToken);

        expect(response.statusCode, `Status code should be 500`).to.be.equal(500);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);     
    });

    it(`Delete user`, async () => {
        let response = await users.deleteUser(userId, accessToken);

        expect(response.statusCode, `Status code should be 204`).to.be.equal(204);
        expect(response.timings.phases.total, `Response time should be less than 1s`).to.be.lessThan(1000);  
    });   
});
